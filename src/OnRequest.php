<?php

declare(strict_types=1);

namespace Kiri\Router;

use Kiri\Abstracts\CoordinatorManager;
use Kiri\Coordinator;
use Kiri\Di\Inject\Container;
use Kiri\Di\Context;
use Kiri\Di\Interface\ResponseEmitterInterface;
use Kiri\Router\Base\ExceptionHandlerDispatcher;
use Kiri\Router\Constrict\ConstrictRequest as CQ;
use Kiri\Router\Constrict\ConstrictResponse;
use Kiri\Router\Interface\ExceptionHandlerInterface;
use Kiri\Router\Interface\OnRequestInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Throwable;

/**
 * OnRequest event
 */
class OnRequest implements OnRequestInterface
{

    /**
     * @var RouterCollector
     */
    public RouterCollector $router;


    /**
     * @var ExceptionHandlerInterface
     */
    public ExceptionHandlerInterface $exception;


    /**
     * @var ResponseEmitterInterface
     */
    public ResponseEmitterInterface $responseEmitter;


    /**
     * @var ConstrictResponse
     */
    #[Container(ConstrictResponse::class)]
    public ConstrictResponse $constrictResponse;


    /**
     * @param ResponseInterface $response
     * @param DataGrip $dataGrip
     */
    public function __construct(public ResponseInterface $response, DataGrip $dataGrip)
    {
        $this->responseEmitter = $this->response->emmit;
        $exception             = \config('exception.http');
        if (!in_array(ExceptionHandlerInterface::class, class_implements($exception))) {
            $exception = ExceptionHandlerDispatcher::class;
        }
        $this->exception = \Kiri::getDi()->get($exception);
        $this->router    = $dataGrip->get(ROUTER_TYPE_HTTP);
    }


    /**
     * @param Request $request
     * @param Response $response
     * @throws
     */
    public function onRequest(Request $request, Response $response): void
    {
        try {
            /** @var CQ $PsrRequest */
            Context::set(ResponseInterface::class, new ConstrictResponse($this->response->contentType));
            $PsrRequest = Context::set(RequestInterface::class, CQ::builder($request));

            CoordinatorManager::utility(Coordinator::WORKER_START)->yield();

            $PsrResponse = $this->router->query($request->server['path_info'], $request->getMethod())->run($PsrRequest);
        } catch (Throwable $throwable) {
            $PsrResponse = $this->exception->emit($throwable, $this->constrictResponse);
        } finally {
            $this->responseEmitter->response($PsrResponse, $response, $PsrRequest);
        }
    }

}

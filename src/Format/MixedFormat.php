<?php

namespace Kiri\Router\Format;

use Kiri;
use Kiri\Router\Constrict\Stream;
use Psr\Http\Message\ResponseInterface;

class MixedFormat implements IFormat
{


	/**
	 * @param ResponseInterface $response
	 */
	public function __construct(public ResponseInterface $response)
	{
	}


	/**
     * @param mixed $result
     * @return ResponseInterface
     */
    public function call(mixed $result): ResponseInterface
    {
        if ($result instanceof ResponseInterface) {
            return $result;
        }
        if (is_object($result)) {
            return $this->response->withBody(new Stream('[object]'));
        }
        if (is_array($result)) {
            return $this->response->withBody(new Stream(json_encode($result, JSON_UNESCAPED_UNICODE)));
        } else {
            return $this->response->withBody(new Stream((string)$result));
        }
    }


}
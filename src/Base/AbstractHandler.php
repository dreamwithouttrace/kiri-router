<?php
declare(strict_types=1);

namespace Kiri\Router\Base;

use Kiri\Router\Handler;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;

abstract class AbstractHandler
{


	public int $offset = 0;


	/**
	 * @param array   $middlewares
	 * @param Handler $handler
	 *
	 * @throws
	 */
	public function __construct(public array $middlewares, public Handler $handler)
	{
	}


	/**
	 * @param ServerRequestInterface $request
	 *
	 * @return ResponseInterface
	 * @throws
	 */
	public function execute(ServerRequestInterface $request): ResponseInterface
	{
		if (!isset($this->middlewares[$this->offset])) {
			return $this->handler->handle($request);
		}

		$middleware   = $this->middlewares[$this->offset];
		$this->offset += 1;

		if (!($middleware instanceof MiddlewareInterface)) {
			$middleware = \Kiri::getDi()->get($middleware);
		}

		return $middleware->process($request, $this);
	}

}

<?php
declare(strict_types=1);

namespace Kiri\Router\Base;


use Kiri;
use Kiri\Router\Request;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class WebController
 * @package Kiri\Web
 * @property RequestInterface $request
 * @property ResponseInterface $response
 * @property ContainerInterface $container
 * @property Kiri\Error\StdoutLogger $logger
 */
class Controller extends Kiri\Abstracts\Component
{


    /**
     * @param Request $request
     * @return true
     */
    public function beforeAction(RequestInterface $request): bool
    {
        return true;
    }


    /**
     * @param string $name
     * @return mixed|ContainerInterface|RequestInterface|ResponseInterface|LoggerInterface
     * @throws \Exception
     */
    public function __get(string $name)
    {
        return match ($name) {
            'request'   => di(RequestInterface::class),
            'response'  => di(ResponseInterface::class),
            'container' => di(ContainerInterface::class),
            'logger'    => di(LoggerInterface::class),
            default     => parent::__get($name)
        }; // TODO: Change the autogenerated stub
    }
}

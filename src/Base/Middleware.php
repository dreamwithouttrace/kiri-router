<?php
declare(strict_types=1);

namespace Kiri\Router\Base;

use Kiri;
use Kiri\Router\Validator\Validator;
use Kiri\Router\Validator\ValidatorMiddleware;

class Middleware
{


    /**
     * @var array
     */
    protected static array $mapping = [];


    protected static array $validators = [];

    /**
     * @param string $className
     * @param string $method
     * @param string $middleware
     * @return void
     */
    public static function set(string $className, string $method, string $middleware): void
    {
        $path = $className . '::' . $method;
        if (!isset(static::$mapping[$path])) {
            static::$mapping[$path] = [];
        }
        if (!in_array($middleware, static::$mapping[$path])) {
            static::$mapping[$path][] = $middleware;
        }
    }


    /**
     * @param string $className
     * @param string $method
     * @return array
     */
    public static function get(string $className, string $method): array
    {
        return static::$mapping[$className . '::' . $method] ?? [];
    }


    /**
     * @param string $class
     * @param string $method
     * @param Validator $validators
     */
    public static function setValidator(string $class, string $method, Validator $validators): void
    {
        self::$validators[$class . '::' . $method] = $validators;
    }


    /**
     * @param string $class
     * @param string $method
     * @return Validator|null
     */
    public static function getValidator(string $class, string $method): ?Validator
    {
        return static::$validators[$class . '::' . $method] ?? null;
    }
}

<?php
declare(strict_types=1);

namespace Kiri\Router;

use Closure;
use Exception;
use Kiri;
use Kiri\Router\Format\ArrayFormat;
use Kiri\Router\Format\MixedFormat;
use Kiri\Router\Format\OtherFormat;
use Kiri\Router\Format\ResponseFormat;
use Kiri\Router\Format\VoidFormat;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;

class ControllerInterpreter
{


    /**
     * @param object $class
     * @param string|ReflectionMethod $method
     * @param ReflectionClass|null $reflection
     * @return Handler
     * @throws
     */
    public function addRouteByString(object $class, string|ReflectionMethod $method, ?ReflectionClass $reflection = null): Handler
    {
        if (is_null($reflection)) {
            $reflection = Kiri::getDi()->getReflectionClass($class::class);
        }
        return $this->resolveMethod($class, $method, $reflection);
    }


    /**
     * @param Closure $method
     * @return Handler
     * @throws
     */
    public function addRouteByClosure(Closure $method): Handler
    {
        $reflection = new ReflectionFunction($method);

        $parameters = Kiri::getDi()->getFunctionParams($method);

        $returnType = $this->getReturnType($reflection);

        return new Handler($method, $parameters, $returnType);
    }


    /**
     * @param object $class
     * @param string|ReflectionMethod $method
     * @param ReflectionClass|null $reflection
     * @return Handler
     * @throws
     */
    public function addRouteByObject(object $class, string|ReflectionMethod $method, ?ReflectionClass $reflection = null): Handler
    {
        if (is_null($reflection)) {
            $reflection = Kiri::getDi()->getReflectionClass($class::class);
        }
        return $this->resolveMethod($class, $method, $reflection);
    }


    /**
     * @param object $class
     * @param string|ReflectionMethod $reflectionMethod
     * @param ReflectionClass $reflectionClass
     * @return Handler
     * @throws
     */
    public function resolveMethod(object $class, string|\ReflectionMethod $reflectionMethod, ReflectionClass $reflectionClass): Handler
    {
        if (is_string($reflectionMethod)) {
            $reflectionMethod = $reflectionClass->getMethod($reflectionMethod);
        }

        $parameters = Kiri::getDi()->getMethodParams($reflectionMethod);
        $returnType = $this->getReturnType($reflectionMethod);

        return new Handler([$class::class, $reflectionMethod->getName()], $parameters, $returnType);
    }


    /**
     * @param ReflectionMethod|ReflectionFunction $reflectionMethod
     * @return string
     */
    protected function getReturnType(ReflectionMethod|ReflectionFunction $reflectionMethod): string
    {
        return match ($reflectionMethod->getReturnType()?->getName()) {
            'array'                 => ArrayFormat::class,
            'mixed', 'object'       => MixedFormat::class,
            'int', 'string', 'bool' => OtherFormat::class,
            'void'                  => VoidFormat::class,
            default                 => ResponseFormat::class
        };
    }

}
